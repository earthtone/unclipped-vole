import React from 'react'
import { fromEvent } from 'rxjs'
import { usePostMessage } from '@earthtone/react-hooks'
import InputForm from './components/InputForm'
import PluginFrame from './components/PluginFrame'
import {filter} from 'rxjs/operators'

function App () {
  const [appState, updateAppState] = usePostMessage({ initialValue: '[]', frameSelector: 'iframe' })
  const message$ = fromEvent<MessageEvent<string>>(window, 'message');
  React.useEffect(() => {
    message$.pipe(
      filter((m: MessageEvent<unknown>) =>
        m.origin.includes('localhost')
        && !(m.data as any)?.source?.includes('react')
        && Boolean(m.data)
      )
    ).subscribe({
      next (m: MessageEvent<unknown>) {
        updateAppState(JSON.stringify(m.data))
      }
    })
  }, [ message$ ])

  function formEmitter (d: unknown) : void {
    let data = JSON.parse(appState).concat([JSON.parse(d as string)])
    updateAppState(JSON.stringify(data))
  }

  return (
    <React.Fragment>
      <InputForm emit={formEmitter}>
        <PluginFrame url="http://localhost:3001"/>
      </InputForm>
    </React.Fragment>
  )
}

export default App
