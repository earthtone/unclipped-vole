import React from "react"
import * as Styled from './styles'
import { useSubscription } from './hooks'
import { fromMessageData, parseFormData } from './helpers'
import { EmitFn } from '../../App'

export default function InputForm ({ children, emit } : { children: React.ReactNode, emit: EmitFn }) {
  let [, updateSocket] = useSubscription({
    next (m: unknown) {
      emit(fromMessageData(m as MessageEvent<string>))
    },
    error (e: unknown) {
      console.error(e)
    },
    complete () {
      console.log('Done')
    }
  })

  function submitHandler (event: React.FormEvent) : void {
    event.preventDefault()
    let data = parseFormData(event.target as HTMLFormElement)
    updateSocket(data)
    console.log('%cupdate socket', 'padding: 0.5rem; font-weight: 900; text-transform: uppercase;')
  }

  return (
    <React.Fragment>
      <Styled.Main>{children}</Styled.Main>
      <Styled.Form onSubmit={submitHandler}>
        <Styled.TextInput name="value"/>
        <Styled.Button type="submit">submit</Styled.Button>
      </Styled.Form>
    </React.Fragment>
  )
}
