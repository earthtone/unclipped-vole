import React from 'react'
import { useWebSocketStream } from '@earthtone/react-hooks'
import { Observer } from 'rxjs'
import {WebSocketSubject} from 'rxjs/webSocket'

export function useSubscription (subscription: Partial<Observer<unknown>>) : [WebSocketSubject<unknown>, <T,>(d:T) => void]{
  let [socket$, update] = useWebSocketStream()

  React.useEffect(() => {
    let sub$ = socket$.subscribe(subscription)
    return () => {
      sub$.unsubscribe()
    }
  }, [socket$])

  return [socket$, update]
}
