import React from 'react';
import styled from '@emotion/styled'

export const TextInput = styled.textarea`
  border: 1px dashed #888;
  margin: 1rem;
  padding: 0.5rem;
  resize: none;
  height: 200px;
`

export const Button = styled.button`
  background-color: #000;
  color: #fff;
  padding: 0.5rem;
  border-radius: 1rem;
`
export const Form = styled.form`
  display: grid;
  * {
    margin: 0 auto;
  }
`
export const Main = styled.main`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #c6eaee;
  min-height: 300px;
`

