// Form Stuff

export function fromMessageData (message: MessageEvent) {
  return JSON.parse(message.data)
}

export function getData ({ data }: { data: any }) {
  return data
}

export function parseFormData (form: HTMLFormElement) : string {
  let data = formDataToList(new FormData(form)).reduce(foldToObject, {} as [string, FormDataEntryValue])
  return JSON.stringify(data)
}

export function formDataToList (data: FormData) : [string, FormDataEntryValue][]{
  return Array.from(data.entries())
}

export function foldToObject (a: [string, FormDataEntryValue], [k,v]:[string, FormDataEntryValue]) {
  return ({ ...a, [k]: v })
}

