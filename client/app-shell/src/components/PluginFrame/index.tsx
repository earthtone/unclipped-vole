import styled from '@emotion/styled'
import React from 'react'

const Frame = styled.iframe`
  width: 100%;
  height: 100%;
  overflow: hidden;
`
export default function PluginFrame ({ url }: { url: string }) {
  return (
    <Frame src={url} />
  )
}
