import React from 'react'
import { fromEvent } from 'rxjs'
import styled from '@emotion/styled'

export const Main = styled.main`
  width: 100%;
  height: 100%;
  display: grid;
`
export const Button = styled.button`
  background-color: #000;
  color: #fff;
  padding: 0.5rem;
  border-radius: 1rem;
`
function App () {
  const [state, setState] = React.useState('[]')

  const message$ = fromEvent<MessageEvent<string>>(window, 'message');
  const emit = React.useCallback((m: unknown) => window.parent.postMessage(m, '*'), [window.parent])

  React.useEffect(() => {
    message$.subscribe({
      next(m) {
        if (m.data) {
          setState(m.data)
        }
      },
      error (e) {
        console.error(e)
      },
      complete () {
        console.log('complete')
      }
    })
    console.groupEnd()
  }, [])

  return (
    <Main>
      <pre><code>{state}</code></pre>
      <Button onClick={clickHandler}>click me</Button>
    </Main>
  )

  function clickHandler () {
    console.log('%cclick', 'font-weight: 900;text-transform:uppercase;')
    let newState = JSON.parse(state)
    newState = newState.concat([{ value: 'updated from within frame' }])
    emit(JSON.stringify(newState))
  }
}

export default App
