import websocket from 'ws'

import {fromEvent} from 'rxjs'
import {mergeMap, mergeWith, tap} from 'rxjs/operators'

type Message = { type: string, data?: any }

const wss = new websocket.Server({ port: 8080 })
const subscription = {
  next: (x: any) => {
    console.log('CONN', toMessage(x))
  },
  error: (e: Error) => {
    console.error('ERR', e)
  },
  complete: () => {
    console.log('DONE')
  }
}

fromEvent(wss, 'connection')
  .pipe(mergeMap(mergeStreams))
  .subscribe(subscription)

function mergeStreams (ws: unknown) {
  let msg$ = fromEvent(ws as EventTarget, 'message')
  let end$ = fromEvent(ws as EventTarget, 'close')
  return msg$.pipe(
    tap(echo(ws as WebSocket[])),
    mergeWith(end$),
  )
}

function echo (ws: WebSocket[]) : (m: Message) => void {
  return (m) => {
    head(ws).send(serialize(m))
  }
}

function toMessage (x: any) : Message {
  return { type: x?.type || 'none', data: x?.data }
}

function serialize (x:any) {
  return JSON.stringify(toMessage(x))
}

function head <T>(xs: T[]): T {
  return xs[0]
}
