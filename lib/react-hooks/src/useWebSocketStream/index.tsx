import { webSocket, WebSocketSubject } from 'rxjs/webSocket'

export const DEFAULT_SOCKET_URL = 'ws://localhost:8080'

export function useWebSocketStream (url = DEFAULT_SOCKET_URL) : [WebSocketSubject<unknown>, <T,>(data: T) => void] {
  var socket$ = webSocket(url)
  var update = <T,>(data: T) => socket$.next(data)
  return [socket$, update]
}

