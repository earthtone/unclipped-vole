import React from 'react'

export function usePostMessage <T> ({ initialValue, frameSelector }:{ initialValue: T, frameSelector: string }) : [T, React.Dispatch<React.SetStateAction<T>>] {
  let child = React.useRef<Window|null>(null)
  let [state, setState] = React.useState(initialValue)

  let sendMessage = React.useCallback(() => {
    child.current = (document.querySelector(frameSelector) as HTMLIFrameElement)?.contentWindow || null
    child.current?.postMessage(state, '*')
  }, [child.current, state])

  React.useEffect(() => {
    let timeout: ReturnType<typeof setTimeout> = setTimeout(() => {
      sendMessage()
    }, 0)

    return () => {
      clearTimeout(timeout)
      child.current = null
    }
  }, [state])

  return [state, setState]
}
